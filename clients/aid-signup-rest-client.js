'use strict';

const config = require('../../configuration');
const httplease = require('httplease');

const baseUrl = config.signup.baseUrl;

class AidSignupRestClient {

    constructor({cookieName}) {
        this.cookieName = cookieName;
        this.httpClient = httplease.builder()
            .withBaseUrl(baseUrl)
            .withTimeout(config.signup.timeout || 30000)
            .withBufferBodyResponseHandler()
            .withHeaders({'Content-Type': 'application/json'});
    }

    async getAuthCookie({token}) {

        const path = '/login';

        console.log(`[AidSignupRestClient] GET ${baseUrl}${path}?token=${token}`);

        return await this.httpClient
            .withPath(path)
            .withParams({token: token})
            .withMethodGet()
            .withExpectStatus([200, 302])
            .send()
            .then((response) => [].concat([], ...response.getHeader('Set-Cookie')
                .filter((setCookie) => {
                    return new RegExp(this.cookieName).test(setCookie);
                })
                .map((authCookie) => {
                    return authCookie.split(';')
                        .filter((cookieParam) => {
                            return new RegExp(this.cookieName).test(cookieParam);
                        });

                }))[0]
            );
    }

    async logout() {
        const path = '/logout';

        console.log(`[AidSignupRestClient] GET ${baseUrl}${path}`);

        return await this.httpClient
            .withPath(path)
            .withMethodGet()
            .withExpectStatus([200, 302, 303])
            .send();
    }
}

module.exports = {AidSignupRestClient};
